jQuery(document).ready(function () {
    function bgImage() {
        jQuery('.bg-image').each(function () {
            var $node = jQuery(this);

            if ($node.hasClass('img-added')) {
                return false;
            }
            $node.addClass('img-added');
            var imgUrl = $node.data('bgimage');
            $node.css({
                backgroundImage: "url('" + imgUrl + "')"
            });
        });
    }
    function cardReviews() {
        var options = {
            max_value: 5,
            step_size: 1,
            initial_value: 4
        }
        var cardOptions = {
            max_value: 5,
            step_size: 1,
            readonly: true,
            initial_value: 4
        }

        jQuery('.rater').rate(options);
        jQuery('.rater--disabled').rate(cardOptions);

    }

    function textareaAutosize() {
        jQuery('.form__control--autosize').each(function () {

            autosize(jQuery(this));

        });
    }


    bgImage();
    cardReviews();
    textareaAutosize();
});